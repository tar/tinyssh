#!/bin/sh
# 20210320
# Jan Mojzis
# Public domain.

# change directory to $AUTOPKGTEST_TMP
cd "${AUTOPKGTEST_TMP}"

# we need tools from /usr/sbin
PATH="/usr/sbin:${PATH}"
export PATH

# backup ~/.ssh
rm -rf ~/.ssh.tinysshtest.bk
[ -d ~/.ssh ] && mv ~/.ssh ~/.ssh.tinysshtest.bk
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# run tinysshd on port 10000
rm -rf sshkeydir
tinysshd-makekey -q sshkeydir
tcpserver -HRDl0 127.0.0.1 10000 tinysshd -- sshkeydir 2>tinysshd.log &
tcpserverpid=$!

cleanup() {
  ex=$?
  rm -rf ~/.ssh sshkeydir testfile1 ~/testfile2 testfile3 tinysshd.log
  [ -d ~/.ssh.tinysshtest.bk ] && mv ~/.ssh.tinysshtest.bk ~/.ssh
  #kill tcpserver
  kill -TERM "${tcpserverpid}" 1>/dev/null 2>/dev/null || :
  kill -KILL "${tcpserverpid}" 1>/dev/null 2>/dev/null || :
  exit "${ex}"
}
trap "cleanup" EXIT TERM INT

# create authorization keys
ssh-keygen -t ed25519 -q -N '' -f ~/.ssh/id_ed25519 || exit 10
cp -pr ~/.ssh/id_ed25519.pub ~/.ssh/authorized_keys || exit 11

# add server key to the known_hosts
ssh-keyscan -H -p 10000 127.0.0.1 2>/dev/null > ~/.ssh/known_hosts

# get KEXs supported by openssh-client
kexs=""
for kex in curve25519-sha256 curve25519-sha256@libssh.org sntrup761x25519-sha512@openssh.com; do
  if [ x"`ssh -o KexAlgorithms=${kex} 2>&1 | grep usage`" != x ]; then
    kexs="${kexs} ${kex}"
  fi
done

for kex in ${kexs}; do

  # create random file
  rm -f testfile1 testfile2 testfile3
  dd if=/dev/urandom of=testfile1 bs=1 count=1048576 2>/dev/null

  ssh -o "KexAlgorithms=${kex}" -p 10000 127.0.0.1 "cat >testfile2" < testfile1
  ssh -o "KexAlgorithms=${kex}" -p 10000 127.0.0.1 "cat testfile2" > testfile3

  if [ x"`sha256sum < testfile1`" = x"`sha256sum < testfile3`" ]; then
    echo "ssh kex ${kex} ok"
  else
    echo "ssh kex ${kex} failed" >&2
    exit 2
  fi
done

exit 0
